#ifndef FLUIDSIM_H
#define FLUIDSIM_H

#include "array2.h"
#include "pcg_solver.h"
#include "sparse_matrix.h"
#include "vec.h"

#include <vector>

//
// TODO: run a marching triangles and view the fluid.
// TODO: write to paraview.
//
class FluidSim
{

public:
  void initialize(float width, int ni_, int nj_);
  void set_boundary(float (*phi)(const Vec2f &));
  void advance(float dt);

  // Grid dimensions
  int ni, nj;
  float dx;

  // Fluid velocity
  Array2f u, v;
  Array2f temp_u, temp_v;

  // Static geometry representation
  Array2f nodal_solid_phi;
  Array2f u_weights, v_weights;
  Array2c u_valid, v_valid;

  Array2f liquid_phi;

  std::vector<Vec2f> particles; // For marker particle simulation
  float particle_radius;

  // Data arrays for extrapolation
  Array2c valid, old_valid;

  // Solver data
  PCGSolver<double> solver;
  SparseMatrixd matrix;
  std::vector<double> rhs;
  std::vector<double> pressure;

  //
  // Interpolate velocity at a certain point
  // Obviously, takes advantage of the structured grid.
  //
  Vec2f get_velocity(const Vec2f & position);

  void add_particle(const Vec2f & position);

private:
  Vec2f trace_rk2(const Vec2f & position, float dt);

  //
  // Move partices by second order RK
  //
  void advect_particles(float dt);

  void compute_phi();

  // Find the maximum cfl number
  // CFL = u dt / dx
  // Don't take steps larger than CFL (or maybe even CFL/2)?
  float cfl();

  //
  // Updates v \nablda v = 0
  // TODO: test what happens if you use first order upwind.
  //       also test cutmull-rum with clipping
  //   
  void advect(float dt);

  void add_force(float dt);

  void project(float dt);
  void compute_weights();
  void solve_pressure(float dt);

  void constrain_velocity();
};

#endif
