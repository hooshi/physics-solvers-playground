# Khalid Jawed
# Figure 4E (Decay length vs. Epsilon)

import numpy as n

Eps = n.logspace(-1, -0.22, 20)

f = open('Commands.txt','w')

# Lgb = 3.3 cm, H = 50 cm
for p in Eps:
    v = 1.0 / (1.0 - p)
    cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 nozzle-speed '+ str(v) + ' fall-height 50\n'
    f.write(cmdline)

# Lgb = 3.3 cm, H = 110 cm
for p in Eps:
    v = 1.0 / (1.0 - p)
    cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 nozzle-speed '+ str(v) + ' fall-height 110\n'
    f.write(cmdline)

# Lgb = 1.8 cm, H = 50 cm
for p in Eps:
    v = 1.0 / (1.0 - p)
    cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options2.txt -- render 0 paused 0 nozzle-speed '+ str(v) + ' fall-height 110\n'
    f.write(cmdline)

f.close()
