# Khalid Jawed
# Figure 4F (lambda_0 vs L_{gb})

import numpy as n

R0 = 0.1605
Y0 = 1.3e7
rho0 = 1.2
g = 981.0

barHarr = [15.0, 61.0, 121.0] # nondimensional heights

radius = ( n.linspace(0.05**(2./3.), 0.6**(2./3.), 20) )**(3./2.) #equally space-out radius along L_{gb} axis
youngM = ( n.linspace( (0.05e7)**(1./3.), 30.0e7**(1./3.), 20) )**3.0 #equally space-out Young's Modulus along L_{gb} axis
density = ( n.linspace(0.05**(-1./3.), 25.0**(-1./3.), 20) )**(-3.) #equally space-out density along L_{gb} axis


f = open('Commands.txt','w')

for barH in barHarr: #loop over height
    for r in radius: #loop over radius
        Lgb = ( r*r*Y0 / (8.0*rho0*g) )**(1.0/3.0)
        rodLen = Lgb/3.3
        dt = 0.02 * rodLen
        if dt > 0.02:
            dt = 0.02
        h = Lgb * barH
        cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 ' \
     		+ ' major-radius ' + str(r) +  ' minor-radius '+ str(r) + ' rod-length ' + str(rodLen) +\
     		' jitter-amplitude 0 fall-height ' + str(h) + ' cutting-point ' + str(h+200) + ' dt ' + str(dt) +'\n'
        f.write(cmdline)

    for y in youngM: #loop over Young's modulus
        Lgb = ( R0*R0*y / (8.0*rho0*g) )**(1.0/3.0)
        rodLen = Lgb/3.3
        dt = 0.02 * rodLen
        if dt > 0.02:
            dt = 0.02
        h = Lgb * barH
     	cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 ' \
     		+ ' youngs-modulus ' + str(y) +  ' shear-modulus '+ str(y/3.0) + ' rod-length ' + str(rodLen) +\
     		' jitter-amplitude 0 fall-height ' + str(h) + ' cutting-point ' + str(h+200) + ' dt ' + str(dt) +'\n'
        f.write(cmdline)

    for d in density: #loop over density
        Lgb = ( R0*R0*Y0 / (8.0*d*g) )**(1.0/3.0)
        rodLen = Lgb/3.3
        dt = 0.02 * rodLen
        if dt > 0.02:
            dt = 0.02
        h = Lgb * barH
        cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 ' \
            + ' density ' + str(d) + ' rod-length ' + str(rodLen) +\
            ' jitter-amplitude 0 fall-height ' + str(h) + ' cutting-point ' + str(h+200) + ' dt ' + str(dt) +'\n'
        f.write(cmdline)

f.close()
