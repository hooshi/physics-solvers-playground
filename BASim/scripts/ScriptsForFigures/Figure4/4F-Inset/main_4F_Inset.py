# Khalid Jawed
# Figure 4F Inset (lambda_0 vs. deployment height)

import numpy as n

height = n.logspace(0.0, 3.0, 20)

f = open('Commands.txt','w')

for h in height:
    
    rodLen = 1.0 #spatial discretization
    if h < 10.0*rodLen: #redundant most likely
        rodLen = h/10.0
    else:
        rodLen = 1.0
    
    cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 rod-length ' + str(rodLen) +\
        ' jitter-amplitude 0.0 fall-height ' + str(h) + ' cutting-point ' + str(h+200) +' dt ' + str(0.02*rodLen) + '\n'
    f.write(cmdline)

f.close()
