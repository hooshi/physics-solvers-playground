# Khalid Jawed
# Figure 5C (B1, B2, kappa_c vs. L_{gb})

import numpy as n

R0 = 0.1605
Y0 = 1.3e7
rho0 = 1.2
g = 981.0


radius = ( n.linspace(0.05**(2./3.), 0.6**(2./3.), 9) )**(3./2.) #equally space-out radius along L_{gb} axis
youngM = ( n.linspace( (0.05e7)**(1./3.), 30.0e7**(1./3.), 9) )**3.0 #equally space-out Young's Modulus along L_{gb} axis
density = ( n.linspace(0.05**(-1./3.), 25.0**(-1./3.), 9) )**(-3.) #equally space-out density along L_{gb} axis

f = open('Commands.txt','w')

for r in radius:
    Lgb = ( r*r*Y0 / (8.0*rho0*g) )**(1.0/3.0)
      
    rodLen = Lgb/3.3 #spatial discretization
    dt = 0.02 * rodLen #time step
    if dt > 0.02: #redundant most likely
        dt = 0.02

    curvarr = n.linspace(0, 1.0/Lgb, 15) #array of natural curvatures

    for c in curvarr:
        h = Lgb * 15.0
        curv = c*rodLen;
        cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 natural-curvature ' \
        + str(curv) + ' major-radius ' + str(r) +  ' minor-radius '+ str(r) + ' rod-length ' + str(rodLen) +\
        ' jitter-amplitude 0 fall-height ' + str(h) + ' cutting-point ' + str(h+200) + ' dt ' + str(dt) +'\n'
        f.write(cmdline)

for y in youngM:
    Lgb = ( R0*R0*y / (8.0*rho0*g) )**(1.0/3.0)

    rodLen = Lgb/3.3
    dt = 0.02 * rodLen
    if dt > 0.02:
        dt = 0.02
      
    curvarr = n.linspace(0, 1.0/Lgb, 15)

    for c in curvarr:
        h = Lgb * 15.0
        curv = c*rodLen;
        cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 natural-curvature ' \
        + str(curv) + ' youngs-modulus ' + str(y) +  ' shear-modulus '+ str(y/3.0) + ' rod-length ' + str(rodLen) +\
        ' jitter-amplitude 0 fall-height ' + str(h) + ' cutting-point ' + str(h+200) + ' dt ' + str(dt) +'\n'
        f.write(cmdline)

for d in density:
    Lgb = ( R0*R0*Y0 / (8.0*d*g) )**(1.0/3.0)

    rodLen = Lgb/3.3
    dt = 0.02 * rodLen
    if dt > 0.02:
        dt = 0.02

    curvarr = n.linspace(0, 1.0/Lgb, 15)

    for c in curvarr:
        h = Lgb * 15.0
        curv = c*rodLen;
        cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 natural-curvature ' \
        + str(curv) + ' density ' + str(d) + ' rod-length ' + str(rodLen) +\
        ' jitter-amplitude 0 fall-height ' + str(h) + ' cutting-point ' + str(h+200) + ' dt ' + str(dt) +'\n'
        f.write(cmdline)

f.close()
