# Khalid Jawed
# Figure 5D (Critical curvature vs. deployment height)

import numpy as n

f = open('Commands.txt','w')

nondimH = n.logspace(1.0, 2.1, 20) # array of deployment heights

for barH in nondimH: #loop over heights
    rodLen = 1.0 #spatial discretization
    dt = 0.02 #time step
    curvarr = n.linspace(0, 0.3, 20)
    h = 3.302 * barH #Dimensional height for Lgb = 3.302 cm
    
    for curvature in curvarr: # loop over natural curvatures
        curv = curvature*rodLen;
        cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 natural-curvature ' \
            + str(curv) + ' rod-length ' + str(rodLen) +\
            ' jitter-amplitude 0 fall-height ' + str(h) + ' cutting-point ' + str(h+200) + ' dt ' + str(dt) +'\n'
        f.write(cmdline)

f.close()
