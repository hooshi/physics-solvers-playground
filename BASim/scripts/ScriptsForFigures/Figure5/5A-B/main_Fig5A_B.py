# Khalid Jawed
# Figure 5A,B (wavelength, amplitude vs. epsilon)

import numpy as n

curvarr1 = n.linspace(0.0, 40.0, 41) #high curvature
curvarr2 = [3.3, 1.65, 0.66] #low curvature
curvarr = n.concatenate([curvarr1, curvarr2]) # array of curvatures in 1/meter

f = open('Commands.txt','w')

for c in curvarr: #loop over curvatures
    cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 natural-curvature ' \
        + str(c/100.0) + '\n'
    f.write(cmdline)

f.close()
