# Khalid Jawed
# Figure 2C (Static coiling radius vs. natural curvature)

import numpy as n

curvarr = 1/n.linspace(2, 20.0, 20) #Array of natural curvatures

f = open('Commands.txt','w')

# Lgb = 3.3 cm, H = 50 cm
for c in curvarr :
    cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 natural-curvature ' + str(c)+'\n'
    f.write(cmdline)

f.close()
