# Khalid Jawed
# Figure 2D (Length of inversion vs. deployment height)

import numpy as n

height = n.linspace(10.0, 80.0, 15.0) # Array of deployment heights
curvarr = [0.2096, 0.25, 0.30, 0.3654] #Array of natural curvatures

f = open('Commands.txt','w')

for h in height:
      for c in curvarr:
          cmdline = 'Apps/BASimulator/BASimulator -r 1 -f options1.txt -- render 0 paused 0 natural-curvature ' \
          + str(c) + ' fall-height ' + str(h) + ' cutting-point ' + str(h+200) +'\n'
          f.write(cmdline)

f.close()
